-- CreateTable
CREATE TABLE `clients` (
    `id` INTEGER NOT NULL,
    `nom` VARCHAR(50) NULL,
    `prenom` VARCHAR(50) NULL,
    `email` VARCHAR(100) NULL,
    `telephone` VARCHAR(20) NULL,
    `adresse` VARCHAR(100) NULL,

    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `historique_location` (
    `id` INTEGER NOT NULL,
    `moto_id` INTEGER NULL,
    `client_id` INTEGER NULL,
    `date_location` DATE NULL,
    `date_retour` DATE NULL,
    `montant_total` DECIMAL(10, 2) NULL,

    INDEX `client_id`(`client_id`),
    INDEX `moto_id`(`moto_id`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `motos` (
    `id` INTEGER NOT NULL,
    `marque` VARCHAR(50) NULL,
    `modele` VARCHAR(50) NULL,
    `annee` INTEGER NULL,
    `couleur` VARCHAR(50) NULL,
    `puissance` INTEGER NULL,
    `prix_location_jour` DECIMAL(10, 2) NULL,
    `disponible` BOOLEAN NULL,

    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- AddForeignKey
ALTER TABLE `historique_location` ADD CONSTRAINT `historique_location_ibfk_1` FOREIGN KEY (`moto_id`) REFERENCES `motos`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

-- AddForeignKey
ALTER TABLE `historique_location` ADD CONSTRAINT `historique_location_ibfk_2` FOREIGN KEY (`client_id`) REFERENCES `clients`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

