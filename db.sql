CREATE DATABASE IF NOT EXISTS db_microservice;

USE db_microservice;

CREATE TABLE motos (
    id INT PRIMARY KEY,
    marque VARCHAR(50),
    modele VARCHAR(50),
    annee INT,
    couleur VARCHAR(50),
    puissance INT,
    prix_location_jour DECIMAL(10,2),
    disponible BOOLEAN
);
CREATE TABLE clients (
    id INT PRIMARY KEY,
    nom VARCHAR(50),
    prenom VARCHAR(50),
    email VARCHAR(100),
    telephone VARCHAR(20),
    adresse VARCHAR(100)
);
CREATE TABLE historique_location (
    id INT PRIMARY KEY,
    moto_id INT,
    client_id INT,
    date_location DATE,
    date_retour DATE,
    montant_total DECIMAL(10,2),
    FOREIGN KEY (moto_id) REFERENCES motos(id),
    FOREIGN KEY (client_id) REFERENCES clients(id)
);